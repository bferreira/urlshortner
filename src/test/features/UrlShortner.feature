Feature: URL Shortener 

	Most of us are familiar with seeing URLs like bit.ly or t.co on our Twitter or Facebook feeds. 
These are examples of shortened URLs, which are a short alias or pointer to a longer page link. 
For example, I can send you the shortened URL http://bit.ly/SaaYw5 that will forward you to a very long 
Google URL with search results on how to iron a shirt.

Scenario: User wants to shorten an URL
	Given User Make a POST in "/shortenUrl" with payload: 
		"""
			{"url":"http://www.google.com"}
		""" 
	Then User shoudld receive as response "4170157c"
	Then User Make a GET in "/4170157c" should receive as response status 301 and location "http://www.google.com"
	
Scenario: User wants to shorten an empty URL
	Given User Make a POST in "/shortenUrl" with payload: 
		"""
			{"url":""}
		""" 
	Then User shoudld receive error 400 with message: "Url cannot be empty"