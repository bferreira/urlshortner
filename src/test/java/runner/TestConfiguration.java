package runner;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.util.DefaultUriBuilderFactory;

public class TestConfiguration implements ApplicationListener<WebServerInitializedEvent> {
	static final protected String root = "http://localhost:";
	static protected String domain = null;
	@Override
	public void onApplicationEvent(final WebServerInitializedEvent event) {
		domain = root + event.getWebServer().getPort() + "/";
		SpringIntegrationTest.template.setUriTemplateHandler(new DefaultUriBuilderFactory(domain));
	}
}
