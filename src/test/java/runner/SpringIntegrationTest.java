package runner;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import com.shortner.App;

@ContextConfiguration(classes = { App.class, TestConfiguration.class }, loader = SpringBootContextLoader.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpringIntegrationTest {
	@LocalServerPort
	protected int port;
	protected static RestTemplate template = new RestTemplate(new SimpleClientHttpRequestFactory() {
		protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
			super.prepareConnection(connection, httpMethod);
			connection.setInstanceFollowRedirects(false);
		}
	});
	
	protected String domain() {
		return TestConfiguration.domain;
	}
}