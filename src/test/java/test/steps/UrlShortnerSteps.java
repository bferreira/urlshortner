package test.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import runner.SpringIntegrationTest;

public class UrlShortnerSteps extends SpringIntegrationTest {
	private ResponseEntity<String> response;
	private HttpClientErrorException errorResponse;

	@Given("^User Make a POST in \\\"([^\\\"]*)\\\" with payload:$")
	public void user_Make_a_POST_in_with_payload(String endpoint, String payload) throws Throwable {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		doPost(headers, endpoint, payload);
	}

	@Then("^User shoudld receive as response \"([^\"]*)\"$")
	public void user_shoudld_receive_as_response(String response) throws Throwable {
		assertEquals(domain()+response,this.response.getBody().toString());
	}

	@Then("^User Make a GET in \"([^\"]*)\" should receive as response status (\\d+) and location \"([^\"]*)\"$")
	public void user_Make_a_GET_in_should_receive_as_response_status_and_location(String endpoint, int status,
			String fowardUrl) throws Throwable {
		ResponseEntity<String> resp = template.getForEntity(endpoint, String.class);
		assertTrue(resp.getStatusCode().is3xxRedirection());
		assertEquals(fowardUrl, resp.getHeaders().getFirst("Location"));
	}

	@Then("^User shoudld receive error (\\d+) with message: \"([^\"]*)\"$")
	public void user_shoudld_receive_error(int status, String message) throws Throwable {
		assertEquals(status, errorResponse.getRawStatusCode());
		assertEquals(message, errorResponse.getResponseBodyAsString());
	}

	private void doPost(HttpHeaders headers, String endPoint, String payload) throws Exception {
		try {
			response = template.postForEntity(endPoint, new HttpEntity<String>(payload, headers),
					String.class);
		} catch (HttpClientErrorException e) {
			errorResponse = e;
		}
	}

}
