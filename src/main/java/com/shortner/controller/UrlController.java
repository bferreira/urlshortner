package com.shortner.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shortner.controller.dto.UrlRequestBody;
import com.shortner.service.IUrlService;

@RestController
public class UrlController {
	private static final String INVALID_URL_FORMAT = "Invalid url format: ";
	private static final String URL_SHOULD_HAVE_LESS_THEN_1000_CHACTERES = "Url should have less then 1000 chacteres";
	private static final String URL_CANNOT_BE_EMPTY = "Url cannot be empty";
	@Autowired
	private IUrlService urlService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public void fowardToUrl(@PathVariable String id, HttpServletResponse resp) throws Exception {
		final Optional<String> url = urlService.findUrlById(id);
		if (url.isPresent()) {
			resp.addHeader("Location", url.get());
			resp.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	@RequestMapping(value = "/shortenUrl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object shortenUrl(HttpServletRequest httpRequest, @RequestBody UrlRequestBody request) {
		Object response;
		String url = request.getUrl();
		if (StringUtils.isEmpty(url)) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(URL_CANNOT_BE_EMPTY);
		} else
		if (url.length() < 1000 == false) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(URL_SHOULD_HAVE_LESS_THEN_1000_CHACTERES);
		} else if (isUrlValidFormat(url) == false) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(INVALID_URL_FORMAT + url);
		} else {
			String requestUrl = httpRequest.getRequestURL().toString();
            String host = requestUrl.substring(0, requestUrl.indexOf(httpRequest.getRequestURI(),
                "http://".length()));
			response = host + "/" + urlService.shortenUrl(url);
		}
		return response;
	}

	private boolean isUrlValidFormat(String url) {
		boolean valid = true;
		try {
			new URL(url);
		} catch (MalformedURLException e) {
			valid = false;
		}
		return valid;
	}
}
