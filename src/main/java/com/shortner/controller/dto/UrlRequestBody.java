package com.shortner.controller.dto;

public class UrlRequestBody {
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
