package com.shortner.service;

import java.util.Optional;

public interface IUrlService {
    Optional<String> findUrlById(String id);

    String shortenUrl(String url);
    
    String tokenizeUrl(String url);
}
