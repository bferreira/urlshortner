package com.shortner.service;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.hash.Hashing;
import com.shortner.dao.IUrlDAO;
import com.shortner.entity.TokenizedUrl;

@Service
public class HashUrlService implements IUrlService {
	@Autowired
	private IUrlDAO dao;
	
	@Override
	public Optional<String> findUrlById(String id) {
		return dao.findUrlById(id);
	}

	@Override
	public String shortenUrl(String url) {
		String tokenizeUrl = tokenizeUrl(url);
		dao.save(new TokenizedUrl(tokenizeUrl,url));
		return tokenizeUrl;
	}

	@Override
	public String tokenizeUrl(String url) {
		return Hashing.murmur3_32().hashString(url, StandardCharsets.UTF_8).toString();

	}
}
