package com.shortner.dao;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.shortner.entity.TokenizedUrl;

@Component
public class InMemoryUrlDAO implements IUrlDAO {
	private Map<String, String> urlByIdMap = new ConcurrentHashMap<>();
	
	@Override
	public Optional<String> findUrlById(String id) {
		return Optional.ofNullable(urlByIdMap.get(id));
	}

	@Override
	public void save(TokenizedUrl tokenizedUrl) {
		urlByIdMap.put(tokenizedUrl.getTokenizeUrl(), tokenizedUrl.getUrl());
	}

}
