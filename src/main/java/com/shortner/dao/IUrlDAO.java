package com.shortner.dao;

import java.util.Optional;

import com.shortner.entity.TokenizedUrl;

public interface IUrlDAO {
	Optional<String> findUrlById(String id);

	void save(TokenizedUrl tokenizedUrl);
}
