package com.shortner.entity;

public class TokenizedUrl {
	private String tokenizeUrl;
	private String url;

	public TokenizedUrl(String tokenizeUrl, String url) {
		this.tokenizeUrl = tokenizeUrl;
		this.url = url;
	}
	
	public String getTokenizeUrl() {
		return tokenizeUrl;
	}

	public void setTokenizeUrl(String tokenizeUrl) {
		this.tokenizeUrl = tokenizeUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
