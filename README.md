# URL Shortner

To run it using maven
```sh
$ mvn spring-boot:run
```

it will be running at port 8085

# How to use it


*  send a POST to /urlshortner with payload {"url":"http://www.google.com"}

*  it will return a shorten link like this "http://localhost:8085/123abc"

*  Then you can do a GET on http://localhost:8085/123abc, you will be redirect to google home

# To run on docker 


*  to create the image, run that command with maven on the root of the project: install dockerfile:build

*  to run the image: run docker run -p 8085:8085 brunoferreirasilva/urlshortner







